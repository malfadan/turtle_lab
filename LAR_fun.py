from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import time as tm
import cv2 as cv
from kmeans import *
import timeit
import threading

class Mask:
    def __init__(self, cnt, image, params):
        self.cnt = cnt
        self.image = image
        self.params = params

Ref_Vals = {"green" : {"H" : 63, "S" : 100, "V" : 75},
            "blue" : {"H" : 105, "S" : 220, "V" : 90},
            "red" : {"H" : 10, "S" : 220, "V" : 120}}

H_THOLD = 25

def create_seg_mask(source, dest):

    start = timeit.default_timer()

    image = cv.imread(source)
    plt.imshow(cv.cvtColor(image, cv.COLOR_BGR2RGB))
    plt.show()

    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    seg_channels = {"green" : np.array([]), "blue" : np.array([]), "red" : np.array([])}

    for colour, channel in Ref_Vals.items():
        seg_channels[colour] = np.abs(channel["H"] - hsv[:, :, 0]) < H_THOLD
        seg_channels[colour] = np.logical_and(seg_channels[colour] == True, hsv[:, :, 1] > channel["S"])
        seg_channels[colour] = np.logical_and(seg_channels[colour] == True, hsv[:, :, 2] > channel["V"])

    bin_mask = np.logical_or(np.logical_or(seg_channels["green"] == True,
                                           seg_channels["blue"] == True),
                             seg_channels["red"] == True)
    #plt.imshow(bin_mask)
    #plt.show()

    out = cv.connectedComponentsWithStats(bin_mask.astype(np.uint8), 8)
    mask_1 = Mask(out[0], out[1], out[2])
    j = 1
    for i in range(mask_1.cnt-1):
        if np.logical_or(np.abs(mask_1.params[j, 3]/mask_1.params[j, 2] - 5) > 2.5,
                         mask_1.params[j, 4] < 400):
            mask_1.image[mask_1.image == j] = 0
            mask_1.image[mask_1.image > j] -= 1
            mask_1.cnt -= 1
            mask_1.params = np.delete(mask_1.params, j, axis=0)
            j -= 1
        j += 1

    plt.imshow(mask_1.image / mask_1.cnt)
    #plt.imsave(dest, mask_1.image / mask_1.cnt)
    plt.show()

    stop = timeit.default_timer()
    print('Time: ', stop - start)
